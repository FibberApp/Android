package com.arctro.fibber.views.game;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.adapters.PlayerListAdapter;
import com.arctro.fibber.models.Player;

import java.util.ArrayList;

public class GameLobbyFragment extends Fragment {

    RecyclerView playerList;
    PlayerListAdapter playerListAdapter;
    RecyclerView.LayoutManager playerListLayoutManager;

    GameLobbyInterface gameLobbyInterface;

    public GameLobbyFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_game_lobby, container, false);

        Bundle args = getArguments();

        TextView gameCodeTextView = (TextView) root.findViewById(R.id.game_code);
        gameCodeTextView.setText(args.getString("gameCode"));

        playerList = (RecyclerView) root.findViewById(R.id.player_list);
        playerList.setHasFixedSize(true);

        playerListLayoutManager = new LinearLayoutManager(getContext());
        playerList.setLayoutManager(playerListLayoutManager);

        playerListAdapter = new PlayerListAdapter();
        playerList.setAdapter(playerListAdapter);

        playerListAdapter.setPlayers((ArrayList<Player>) args.getSerializable("players"));

        Button startGameButton = (Button) root.findViewById(R.id.start_game_button);
        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameLobbyInterface.onStartGame();
            }
        });

        return root;
    }

    public void setPlayers(ArrayList<Player> players){
        getArguments().putSerializable("players", (ArrayList<Player>) players);
        playerListAdapter.setPlayers(players);
    }

    public void setGameLobbyInterface(GameLobbyInterface gameLobbyInterface) {
        this.gameLobbyInterface = gameLobbyInterface;
    }

    public interface GameLobbyInterface{
        public void onStartGame();
    }
}
