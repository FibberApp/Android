package com.arctro.fibber.views.game;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.adapters.PlayerListAdapter;
import com.arctro.fibber.adapters.PlayerScoreListAdapter;
import com.arctro.fibber.models.PlayerScore;
import com.arctro.fibber.models.PlayerSelectedAnswer;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.supporting.Utils;

import java.util.ArrayList;

public class ScoreboardFragment extends Fragment {

    RecyclerView playerList;
    PlayerScoreListAdapter playerListAdapter;
    RecyclerView.LayoutManager playerListLayoutManager;

    public ScoreboardFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_scoreboard, container, false);

        Bundle args = getArguments();

        playerList = (RecyclerView) root.findViewById(R.id.player_list);
        playerList.setHasFixedSize(true);

        playerListLayoutManager = new LinearLayoutManager(getContext());
        playerList.setLayoutManager(playerListLayoutManager);

        playerListAdapter = new PlayerScoreListAdapter();
        playerList.setAdapter(playerListAdapter);

        playerListAdapter.setPlayerScores((ArrayList<PlayerScore>) args.getSerializable("playerScores"));

        return root;
    }
}
