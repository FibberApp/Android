package com.arctro.fibber.views;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.arctro.fibber.R;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.supporting.Utils;

public class StartupActivity extends AppCompatActivity {

    final int PERMISSION_CALLBACK = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        //Show the answer
        final ImageView iv = (ImageView) findViewById(R.id.arctro_logo);

        ThreadFactory.queue(Threads.UI_NO_LOCK, new Runnable(){
            public void run(){
                //Setup the app
                final String error = setupApp();

                //If there was an error exit
                if(error != null){
                    ThreadFactory.queue(Threads.UI, new Runnable(){
                        public void run(){
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getThis());
                            alertDialogBuilder
                                    .setMessage(error)
                                    .setTitle("Error")
                                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    })
                                    .create();
                        }
                    });

                    return;
                }

                //Wait
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Fade out the logo
                ObjectAnimator fadeOut = ObjectAnimator.ofFloat(iv, "alpha",  1f, 0f);
                fadeOut.setDuration(500);

                final AnimatorSet as = new AnimatorSet();
                as.play(fadeOut);

                ThreadFactory.queue(Threads.UI, new Runnable(){
                    public void run(){
                        as.start();

                        //Start the main activity
                        Utils.UISleep(500, new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(getThis(), MenuActivity.class);
                                startActivity(intent);
                                overridePendingTransition(0,0);
                            }
                        });
                    }
                });
            }
        });
    }

    protected String setupApp(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)){
                //Do something
            }

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, PERMISSION_CALLBACK);
        }

        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CALLBACK: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                return;
            }
        }
    }

    private StartupActivity getThis(){
        return this;
    }
}
