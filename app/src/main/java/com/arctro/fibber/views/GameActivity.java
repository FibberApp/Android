package com.arctro.fibber.views;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.arctro.fibber.R;
import com.arctro.fibber.controllers.MusicService;
import com.arctro.fibber.controllers.network.CommandListener;
import com.arctro.fibber.controllers.network.ResponseListener;
import com.arctro.fibber.controllers.network.ServerRequestController;
import com.arctro.fibber.controllers.network.requests.AcknowledgeRequest;
import com.arctro.fibber.controllers.network.requests.BuildGameRequest;
import com.arctro.fibber.controllers.network.requests.CategorySelectRequest;
import com.arctro.fibber.controllers.network.requests.InputAnswerRequest;
import com.arctro.fibber.controllers.network.requests.SelectAnswerRequest;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.models.Answer;
import com.arctro.fibber.models.Category;
import com.arctro.fibber.models.Command;
import com.arctro.fibber.models.GameState;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.models.PlayerScore;
import com.arctro.fibber.models.PlayerSelectedAnswer;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.models.TimeLimit;
import com.arctro.fibber.supporting.Utils;
import com.arctro.fibber.views.game.AnswerInputFragment;
import com.arctro.fibber.views.game.AnswerInputWaitFragment;
import com.arctro.fibber.views.game.CategoryFragment;
import com.arctro.fibber.views.game.GameLobbyFragment;
import com.arctro.fibber.views.game.PlayerSelectedAnswerFragment;
import com.arctro.fibber.views.game.ScoreboardFragment;
import com.arctro.fibber.views.game.SelectAnswerFragment;
import com.arctro.fibber.views.game.SelectAnswerWaitFragment;
import com.arctro.fibber.views.menu.CreateGameActivity;
import com.arctro.fibber.views.menu.JoinGameActivity;
import com.arctro.fibber.views.menu.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.arctro.fibber.controllers.network.ServerRequestController.server;

/**
 * The main game controller.
 */
public class GameActivity extends AppCompatActivity implements CommandListener, GameLobbyFragment.GameLobbyInterface, CategoryFragment.CategoryInterface, AnswerInputFragment.AnswerInputInterface, SelectAnswerFragment.AnswerInterface{

    //The server endpoint
    ServerRequestController server = ServerRequestController.server;
    //The game canvas
    FrameLayout gameCanvas;

    //The main player
    Player player;
    //All players
    ArrayList<Player> players = new ArrayList<>();
    //The current question
    Question question;
    //The current answer
    Answer answer;

    //The game lobby view
    GameLobbyFragment gameLobbyFragment = new GameLobbyFragment();

    //The current game state
    GameState gameState = GameState.LOBBY;

    /**
     * Listens for commands from the server and sends the commands to the correct view
     * @param command The received command
     */
    @Override
    public void receive(Command command) {
        try {
            switch (command.getCommand()) {
                //A player joins
                case "player_join": {
                    players.add(new Player((JSONObject) command.getPayload().get("joined")));
                    //Update the game lobby list of players
                    gameLobbyFragment.setPlayers(players);

                    break;
                }

                //A player leaves
                case "player_leave": {
                    Player player = new Player((JSONObject) command.getPayload().get("left"));
                    players.remove(player);
                    //Update the game lobby list of players
                    gameLobbyFragment.setPlayers(players);

                    //Show a notification stating who left the game
                    if(gameState != GameState.LOBBY) {
                        Snackbar.make(gameCanvas, getString(R.string.player_left_game, player.getName()), Snackbar.LENGTH_LONG).show();
                    }
                    break;
                }

                //A category is selected
                case "category_select": {
                    categorySelect(command.getPayload());

                    break;
                }

                //A question is displayed
                case "question": {
                    answerInput(command.getPayload());

                    break;
                }

                //An answer is selected
                case "select_answer": {
                    selectAnswer(command.getPayload());

                    break;
                }

                //Scoreboard
                case "player_selected_answer": {
                    playerSelectedAnswer(command.getPayload());

                    break;
                }

                //The connection with the server has been closed
                case ServerRequestController.CONN_CLOSED: {
                    //Go back to the main menu
                    Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                    startActivity(intent);
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public GameActivity(){
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        //Ensure the screen stays on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Bundle args = getIntent().getExtras();

        //If the server does not exist force the user to reconnect
        if (server == null) {
            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            startActivity(intent);
        }

        //Register the controller with the server listener
        server.registerCommandListener(this);

        //Find the game canvas
        gameCanvas = (FrameLayout) findViewById(R.id.game_canvas);

        //Set the background
        ImageView background = (ImageView) findViewById(R.id.main_background);
        //Set the background color
        background.getDrawable().setColorFilter(getResources().getColor(R.color.greenDarkestDark), PorterDuff.Mode.MULTIPLY);

        //Set the user
        player = (Player) args.getSerializable("player");
        //List all the players
        players = (ArrayList<Player>) args.getSerializable("players");

        //Check if the activity is new, if old stop function
        if (savedInstanceState != null) {
            return;
        }

        //Start the music
        MusicService.getInstance().play(R.raw.hep_cats);

        //Set this controller to interface with the game lobby
        gameLobbyFragment.setGameLobbyInterface(this);

        //Start the game lobby
        gameLobbyFragment.setArguments(getIntent().getExtras());
        switchFragment(gameLobbyFragment, false);
    }

    /**
     * Called when the back key is pressed. Exits after confirmation
     */
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setTitle("Exit");
        builder.setMessage("Are you sure you want to exit the game?");
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }

    @Override
    public void onPause(){
        super.onPause();
        //Stop the music when the app is closed
        MusicService.getInstance().pause();
        server.client.close();
    }

    /**
     * Called when the "start game" button in the lobby is pressed
     */
    @Override
    public void onStartGame() {
        //Send a build game request
        try {
            new BuildGameRequest()
                    .setEndpoint(server)
                    .broadcast();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show the category select screen
     * @param payload The request
     * @throws JSONException The request was invalid
     */
    public void categorySelect(JSONObject payload) throws JSONException{
        //Switch the game state
        gameState = GameState.CATEGORY_SELECT;

        //Create a variable bundle
        Bundle data = new Bundle();

        //Make a list of all categories in the payload
        JSONArray categoriesJSON = (JSONArray) payload.get("categories");
        ArrayList<Category> categories = new ArrayList<>();
        for(int i = 0; i < categoriesJSON.length(); i++){
            categories.add(new Category((JSONObject) categoriesJSON.get(i)));
        }
        data.putSerializable("categories", categories);

        //Put in the selecting player
        Player selector = new Player((JSONObject) payload.get("selector"));
        data.putSerializable("selector", selector);

        //Put in the time limit
        TimeLimit timeLimit = new TimeLimit((JSONObject) payload.get("timeLimit"), server.getServerTimeDifference());
        data.putSerializable("timeLimit", timeLimit);

        //Check if the player is the one selecting the category
        data.putBoolean("pick", payload.has("pick") && payload.getBoolean("pick"));

        //Create a new category fragment, and set this controller to it's interface
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setCategoryInterface(this);

        //Pass in the variable bundle and switch to the fragment
        categoryFragment.setArguments(data);
        switchFragment(categoryFragment);
    }

    /**
     * Called when a category is selected in the category selection view
     * @param category
     */
    @Override
    public void onCategorySelected(Category category) {
        //Broadcast the selection
        try {
            new CategorySelectRequest()
                    .setCategoryId(category.getCategoryId())
                    .setEndpoint(server)
                    .broadcast();
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Show the answer input view
     * @param payload The request
     * @throws JSONException The request was invalid
     */
    public void answerInput(JSONObject payload) throws JSONException{
        //Set the game state
        gameState = GameState.ANSWER_INPUT;

        //Create a variable bundle
        Bundle data = new Bundle();

        //Put in the question
        question = new Question((JSONObject) payload.get("question"));
        data.putSerializable("question", question);

        //Put in the time limit
        TimeLimit timeLimit = new TimeLimit((JSONObject) payload.get("timeLimit"), server.getServerTimeDifference());
        data.putSerializable("timeLimit", timeLimit);

        //Create a new answer input fragment, and set this controller to it's interface
        AnswerInputFragment answerInputFragment = new AnswerInputFragment();
        answerInputFragment.setAnswerInputInterface(this);

        //Pass in the variable bundle and switch to the fragment
        answerInputFragment.setArguments(data);
        switchFragment(answerInputFragment);
    }

    /**
     * Called when an answer is inputted into Input Answer
     * @param answer The inputted answer
     * @param timeLimit The remaining time limit
     * @param startLimit The time limit at the start
     */
    @Override
    public void onAnswerInput(String answer, TimeLimit timeLimit, int startLimit) {
        try {
            //Send the answer to the server
            new InputAnswerRequest()
                    .setAnswer(answer)
                    .setEndpoint(server)
                    .broadcast();

            //Switch the view to the waiting view
            gameState = GameState.ANSWER_INPUT_WAIT;

            Bundle data = new Bundle();
            data.putSerializable("timeLimit", timeLimit);

            data.putInt("startLimit", startLimit);

            this.answer = new Answer("", answer, false);

            AnswerInputWaitFragment answerInputWaitFragment = new AnswerInputWaitFragment();
            answerInputWaitFragment.setArguments(data);
            switchFragment(answerInputWaitFragment);

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Called when the answer input is forfeited
     * @param timeLimit The remaining time limit
     */
    @Override
    public void onForfeit(TimeLimit timeLimit) {

    }

    /**
     * Show the select view
     * @param payload The request
     * @throws JSONException The request was invalid
     */
    public void selectAnswer(JSONObject payload) throws JSONException{
        //Set the game state
        gameState = GameState.SELECT_ANSWER;

        //Change the music
        MusicService.getInstance().play(R.raw.spy_glass);

        //Create a variable bundle
        Bundle data = new Bundle();

        //Make a list of all answers in the payload
        ArrayList<Answer> answers = new ArrayList<>();
        JSONArray answersJ = payload.getJSONArray("answers");
        for(int i = 0; i < answersJ.length(); i++){
            answers.add(new Answer(answersJ.getJSONObject(i)));
        }
        answers.add(answer);
        data.putSerializable("answers", answers);

        //Put in the question
        data.putSerializable("question", question);

        //Put in the time limit
        TimeLimit timeLimit = new TimeLimit((JSONObject) payload.get("timeLimit"), server.getServerTimeDifference());
        data.putSerializable("timeLimit", timeLimit);

        //Create a new select answer fragment, and set this controller to it's interface
        SelectAnswerFragment selectAnswerFragment = new SelectAnswerFragment();
        selectAnswerFragment.setAnswerInterface(this);

        //Pass in the variable bundle and switch to the fragment
        selectAnswerFragment.setArguments(data);
        switchFragment(selectAnswerFragment);
    }

    /**
     * Called when an answer is selected
     * @param answer The selected answer
     * @param timeLimit The remaining time limit
     * @param startLimit The time limit at the start
     */
    @Override
    public void onAnswerSelected(Answer answer, TimeLimit timeLimit, int startLimit) {
        try {
            //Send the select answer request
            new SelectAnswerRequest()
                    .setAnswerId(answer.getAnswerId())
                    .setEndpoint(server)
                    .broadcast();

            //Switch to wait fragment
            gameState = GameState.SELECT_ANSWER_WAIT;

            Bundle data = new Bundle();
            data.putSerializable("timeLimit", timeLimit);

            data.putInt("startLimit", startLimit);

            SelectAnswerWaitFragment answerInputWaitFragment = new SelectAnswerWaitFragment();
            answerInputWaitFragment.setArguments(data);
            switchFragment(answerInputWaitFragment);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Show the player selected answer view
     * @param payload The request
     * @throws JSONException The request was invalid
     */
    public void playerSelectedAnswer(JSONObject payload) throws JSONException{
        //Change the music
        MusicService.getInstance().play(R.raw.hep_cats);

        //Get the relevant part of the payload
        final JSONArray playerSelectedAnswersJSON = payload.getJSONArray("playerSelectedAnswers");
        final JSONArray playerScoresJSON = payload.getJSONArray("playerScores");

        //Process in a non UI thread
        ThreadFactory.queue(Threads.PROCESS, new Runnable() {
            @Override
            public void run() {

                //Make a list of all the player selected answers
                ArrayList<PlayerSelectedAnswer> playerSelectedAnswers = new ArrayList<>();
                for(int i = 0; i < playerSelectedAnswersJSON.length(); i++){
                    try {
                        playerSelectedAnswers.add(new PlayerSelectedAnswer(playerSelectedAnswersJSON.getJSONObject(i)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //Sort the answers
                Collections.sort(playerSelectedAnswers, new PlayerSelectedAnswer.Comparator());

                //For every answer
                for(PlayerSelectedAnswer playerSelectedAnswer: playerSelectedAnswers){
                    //Put in answer
                    final Bundle data = new Bundle();
                    data.putSerializable("playerSelectedAnswer", playerSelectedAnswer);
                    data.putSerializable("question", question);

                    //Show the element in the playerSelectedAnswerFragment
                    ThreadFactory.queue(Threads.UI, new Runnable() {
                        @Override
                        public void run() {
                            PlayerSelectedAnswerFragment playerSelectedAnswerFragment = new PlayerSelectedAnswerFragment();
                            playerSelectedAnswerFragment.setArguments(data);
                            switchFragment(playerSelectedAnswerFragment);
                        }
                    });

                    //Wait for the screen to finish before showing next
                    try {
                        Thread.sleep(8000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                //Make a list of scores
                ArrayList<PlayerScore> playerScores = new ArrayList<>();
                for(int i = 0; i < playerScoresJSON.length(); i++){
                    try {
                        playerScores.add(new PlayerScore(playerScoresJSON.getJSONObject(i)));
                    }catch(JSONException e){
                        e.printStackTrace();
                    }
                }

                //Sort the scores
                Collections.sort(playerScores, new PlayerScore.Comparator());

                //Show the scoreboard
                Bundle data = new Bundle();
                data.putSerializable("playerScores", playerScores);

                ScoreboardFragment scoreboardFragment = new ScoreboardFragment();
                scoreboardFragment.setArguments(data);

                switchFragment(scoreboardFragment);

                //Wait some time
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Tell the server that the scoreboard sequence is over so the next round can start
                try {
                    new AcknowledgeRequest()
                            .setEndpoint(server)
                            .broadcast();
                }catch(JSONException e){
                    e.printStackTrace();
                }
            }
        });
    }

    //Switch a fragment
    public void switchFragment(Fragment f){
        switchFragment(f, true);
    }

    //Switch a fragment with an animation
    public void switchFragment(Fragment f, boolean animate){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(animate) {
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        }
        transaction.replace(R.id.game_canvas, f);
        transaction.commit();
    }
}
