package com.arctro.fibber.views.game;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.models.TimeLimit;
import com.arctro.fibber.supporting.Utils;

public class AnswerInputFragment extends Fragment{

    //The answer input interface
    AnswerInputInterface answerInputInterface;

    public AnswerInputFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_answer_input, container, false);

        //Get the passed in variables
        Bundle args = getArguments();

        //Get the question
        Question question = (Question) args.getSerializable("question");

        //Show the question
        TextView questionDisplay = (TextView) root.findViewById(R.id.question);
        questionDisplay.setText(new SpannableString(Html.fromHtml(Utils.formatUI(question.getQuestion()))), TextView.BufferType.SPANNABLE);

        //Show the time limit
        final TimeLimit timeLimit = (TimeLimit) args.getSerializable("timeLimit");
        final int leftAtStart = (int) timeLimit.getLeft();
        final ProgressBar timeLimitDisplay = (ProgressBar) root.findViewById(R.id.timeLimit);
        timeLimitDisplay.setMax(leftAtStart);
        CountDownTimer timeLimitCount = new CountDownTimer(leftAtStart, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLimitDisplay.setProgress((int) millisUntilFinished);
            }

            @Override
            public void onFinish() {
                timeLimitDisplay.setProgress(0);
            }
        }.start();

        final EditText answerInput = (EditText) root.findViewById(R.id.answer_input);
        answerInput.setOnFocusChangeListener(new Utils.KeyboardCloseOnFocusChange(getActivity()));

        //Send the answer to the answer input interface
        Button enterButton = (Button) root.findViewById(R.id.enter_button);
        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String answer = answerInput.getText().toString();

                if(answer.equals("")){
                    return;
                }

                answerInputInterface.onAnswerInput(answer, timeLimit, leftAtStart);
            }
        });

        return root;
    }

    public void setAnswerInputInterface(AnswerInputInterface answerInputInterface) {
        this.answerInputInterface = answerInputInterface;
    }

    public interface AnswerInputInterface{
        public void onAnswerInput(String answer, TimeLimit timeLimit, int startLimit);
        public void onForfeit( TimeLimit timeLimit);
    }
}
