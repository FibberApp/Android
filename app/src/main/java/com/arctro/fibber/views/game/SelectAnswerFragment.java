package com.arctro.fibber.views.game;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.adapters.AnswerSelectAdapter;
import com.arctro.fibber.models.Answer;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.models.TimeLimit;
import com.arctro.fibber.supporting.Utils;

import java.util.ArrayList;

public class SelectAnswerFragment extends Fragment implements AnswerSelectAdapter.AnswerSelectInterface{

    RecyclerView answerList;
    AnswerSelectAdapter answerListAdapter;
    RecyclerView.LayoutManager answerListLayoutManager;

    int leftAtStart;

    AnswerInterface answerInterface;

    public SelectAnswerFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_select_answer, container, false);

        Bundle args = getArguments();

        answerList = (RecyclerView) root.findViewById(R.id.answer_list);
        answerList.setHasFixedSize(true);

        answerListLayoutManager = new GridLayoutManager(getContext(), 1);
        answerList.setLayoutManager(answerListLayoutManager);

        answerListAdapter = new AnswerSelectAdapter(this);
        answerList.setAdapter(answerListAdapter);

        answerListAdapter.setAnswers((ArrayList<Answer>) args.getSerializable("answers"));

        Question question = (Question) args.getSerializable("question");

        TextView selectAnswerTitle = (TextView) root.findViewById(R.id.select_answer_title);
        selectAnswerTitle.setText(new SpannableString(Html.fromHtml(Utils.formatUI(question.getQuestion()))), TextView.BufferType.SPANNABLE);


        final TimeLimit timeLimit = (TimeLimit) args.getSerializable("timeLimit");
        leftAtStart = (int) timeLimit.getLeft();
        final ProgressBar timeLimitDisplay = (ProgressBar) root.findViewById(R.id.timeLimit);
        timeLimitDisplay.setMax(leftAtStart);
        CountDownTimer timeLimitCount = new CountDownTimer(leftAtStart, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLimitDisplay.setProgress((int) millisUntilFinished);
            }

            @Override
            public void onFinish() {
                timeLimitDisplay.setProgress(0);
            }
        }.start();

        return root;
    }

    public void setAnswerInterface(AnswerInterface answerInterface) {
        this.answerInterface = answerInterface;
    }

    @Override
    public void onAnswerSelected(Answer answer) {
        answerInterface.onAnswerSelected(answer, (TimeLimit) getArguments().getSerializable("timeLimit"), leftAtStart);
    }

    public interface AnswerInterface{
        public void onAnswerSelected(Answer answer, TimeLimit timeLimit, int startLimit);
    }
}
