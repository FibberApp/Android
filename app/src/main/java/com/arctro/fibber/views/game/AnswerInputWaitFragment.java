package com.arctro.fibber.views.game;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.models.TimeLimit;

public class AnswerInputWaitFragment extends Fragment{

    public AnswerInputWaitFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_answer_input_wait, container, false);

        Bundle args = getArguments();

        //Show the time limit
        final TimeLimit timeLimit = (TimeLimit) args.getSerializable("timeLimit");
        final int leftAtStart = (int) timeLimit.getLeft();
        final ProgressBar timeLimitDisplay = (ProgressBar) root.findViewById(R.id.timeLimit);
        timeLimitDisplay.setMax((int) args.getInt("startLimit"));
        CountDownTimer timeLimitCount = new CountDownTimer(leftAtStart, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLimitDisplay.setProgress((int) millisUntilFinished);
            }

            @Override
            public void onFinish() {
                timeLimitDisplay.setProgress(0);
            }
        }.start();

        return root;
    }
}
