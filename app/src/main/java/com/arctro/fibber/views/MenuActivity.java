package com.arctro.fibber.views;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.arctro.fibber.R;
import com.arctro.fibber.controllers.MusicService;
import com.arctro.fibber.views.menu.CreateGameActivity;
import com.arctro.fibber.views.menu.JoinGameActivity;
import com.arctro.fibber.views.menu.LoginActivity;

public class MenuActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //Set the background and mask it with a colour
        ImageView background = (ImageView) findViewById(R.id.main_background);
        background.getDrawable().setColorFilter(getResources().getColor(R.color.greenDarkestDark), PorterDuff.Mode.MULTIPLY);

        //Setup login button
        RelativeLayout loginButton  = (RelativeLayout) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        //Setup join game button
        Button joinGameButton = (Button) findViewById(R.id.join_game_button);
        joinGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), JoinGameActivity.class);
                startActivity(intent);
            }
        });

        //Setup create game button
        Button createGameButton = (Button) findViewById(R.id.host_game_button);
        createGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateGameActivity.class);
                startActivity(intent);
            }
        });

        //Setup tutorial button
        Button tutorialButton = (Button) findViewById(R.id.tutorial_button);
        tutorialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TutorialActivity.class);
                startActivity(intent);
            }
        });

        if(savedInstanceState == null){
            MusicService.createInstance(this);
        }

        MusicService.getInstance().play(R.raw.poppers_and_prosecco);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed(){
        moveTaskToBack(true);
    }

    @Override
    public void onPause(){
        super.onPause();
        MusicService.getInstance().pause();
    }

    @Override
    public void onResume(){
        super.onResume();
        MusicService.getInstance().unpause();
    }
}
