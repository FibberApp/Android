package com.arctro.fibber.views.game;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.adapters.PlayerListAdapter;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.models.PlayerSelectedAnswer;
import com.arctro.fibber.models.Question;
import com.arctro.fibber.supporting.Utils;

public class PlayerSelectedAnswerFragment extends Fragment {

    RecyclerView playerList;
    PlayerListAdapter playerListAdapter;
    RecyclerView.LayoutManager playerListLayoutManager;

    public PlayerSelectedAnswerFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_player_selected_answer, container, false);

        Bundle args = getArguments();

        final PlayerSelectedAnswer playerSelectedAnswer = (PlayerSelectedAnswer) args.getSerializable("playerSelectedAnswer");
        Question question = (Question) args.getSerializable("question");

        playerList = (RecyclerView) root.findViewById(R.id.player_list);
        playerList.setHasFixedSize(true);

        playerListLayoutManager = new LinearLayoutManager(getContext());
        playerList.setLayoutManager(playerListLayoutManager);

        playerListAdapter = new PlayerListAdapter();
        playerList.setAdapter(playerListAdapter);

        playerListAdapter.setPlayers(playerSelectedAnswer.getSelected());

        final RelativeLayout answerButton = (RelativeLayout) root.findViewById(R.id.answer_button);
        final TextView answerButtonText = (TextView) root.findViewById(R.id.answer_button_text);
        answerButtonText.setText(Utils.formatUI(playerSelectedAnswer.getAnswer().getAnswer()));

        TextView questionText = (TextView) root.findViewById(R.id.question);
        questionText.setText(Html.fromHtml(Utils.formatUI(question.getQuestion())));

        MediaPlayer soundEffect = MediaPlayer.create(getContext(), R.raw.drumroll);
        soundEffect.start();

        Utils.UISleep(4000, new Runnable() {
            @Override
            public void run() {
                Log.d("TT", "TTT");

                Animation shrink = AnimationUtils.loadAnimation(getContext(), R.anim.shrink_x);
                answerButton.animate().scaleX(0).setDuration(200).start();

                Utils.UISleep(200, new Runnable() {
                    @Override
                    public void run() {
                        if(playerSelectedAnswer.getAnswerer() == null) {
                            answerButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                            answerButtonText.setText(getResources().getText(R.string.the_truth));

                            MediaPlayer soundEffect = MediaPlayer.create(getContext(), R.raw.tada);
                            soundEffect.start();
                        }else{
                            answerButton.setBackgroundColor(Color.RED);
                            answerButtonText.setText(playerSelectedAnswer.getAnswerer().getName() + "'s answer!");

                            MediaPlayer soundEffect = MediaPlayer.create(getContext(), R.raw.sad_trombone);
                            soundEffect.start();
                        }


                        Animation shrink = AnimationUtils.loadAnimation(getContext(), R.anim.grow_x);
                        answerButton.animate().scaleX(1).setDuration(200).start();
                    }
                });
            }
        });

        return root;
    }
}
