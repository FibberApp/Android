package com.arctro.fibber.views.menu;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.arctro.fibber.supporting.Consts;
import com.arctro.fibber.R;
import com.arctro.fibber.controllers.network.ResponseListener;
import com.arctro.fibber.controllers.network.ServerRequestController;
import com.arctro.fibber.controllers.network.requests.CreateGameRequest;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.models.Command;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.supporting.Utils;
import com.arctro.fibber.views.GameActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class CreateGameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);

        //Set the background and mask it with a colour
        ImageView background = (ImageView) findViewById(R.id.main_background);
        background.getDrawable().setColorFilter(getResources().getColor(R.color.greenDarkestDark), PorterDuff.Mode.MULTIPLY);

        final EditText nameInput = (EditText) findViewById(R.id.name_input);
        nameInput.setOnFocusChangeListener(new Utils.KeyboardCloseOnFocusChange(this));

        //Allow custom server addresses
        final EditText serverInput = (EditText) findViewById(R.id.server_input);
        serverInput.setOnFocusChangeListener(new Utils.KeyboardCloseOnFocusChange(this));
        if(Consts.CUSTOM_SERVER_ALLOWED){
            serverInput.setVisibility(View.VISIBLE);
        }

        Button createGameButton = (Button) findViewById(R.id.create_game_button);
        createGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ThreadFactory.queue(Threads.UI_NO_LOCK, new Runnable(){
                    public void run(){
                        String name = nameInput.getText().toString();

                        if(name.equals("")){
                            return;
                        }

                        try {
                            ServerRequestController.server = new ServerRequestController(new URI((Consts.CUSTOM_SERVER_ALLOWED ? serverInput.getText().toString() : Consts.SERVER_ADDRESS)));
                            if(!ServerRequestController.server.isConnected()){
                                Snackbar.make(v, getString(R.string.error_unable_to_connect), Snackbar.LENGTH_LONG).show();
                                return;
                            }

                            try {
                                new CreateGameRequest()
                                        .setName(name)
                                        .setEndpoint(ServerRequestController.server)
                                        .setResponseListener(new ResponseListener() {
                                            @Override
                                            public void response(Command command) {
                                                try {
                                                    final JSONObject payload = command.getPayload();

                                                    final Player player = new Player((JSONObject) payload.get("player"));
                                                    final ArrayList<Player> players = new ArrayList<Player>();
                                                    players.add(player);

                                                    ThreadFactory.queue(Threads.UI, new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            try {
                                                                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                                                                intent.putExtra("gameCode", payload.getString("gameCode"));
                                                                intent.putExtra("player", player);
                                                                intent.putExtra("players", players);
                                                                startActivity(intent);
                                                            }catch(JSONException e){
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        })
                                        .broadcast();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (URISyntaxException e) {
                            serverInput.setText("");
                            Snackbar.make(v, getString(R.string.error_invalid_url_format), Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
