package com.arctro.fibber.views.game;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.adapters.CategoryListAdapter;
import com.arctro.fibber.models.Category;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.models.TimeLimit;

import java.util.ArrayList;

import static android.view.View.GONE;

public class CategoryFragment extends Fragment implements CategoryListAdapter.CategoryTextListAdapterInterface{

    RecyclerView categoryList;
    CategoryListAdapter categoryListAdapter;
    RecyclerView.LayoutManager categoryListLayoutManager;

    CategoryInterface categoryInterface;

    public CategoryFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_category, container, false);

        Bundle args = getArguments();

        //Show the categories
        categoryList = (RecyclerView) root.findViewById(R.id.category_list);
        categoryList.setHasFixedSize(true);

        categoryListLayoutManager = new GridLayoutManager(getContext(), 1);
        categoryList.setLayoutManager(categoryListLayoutManager);

        categoryListAdapter = new CategoryListAdapter(this);
        categoryList.setAdapter(categoryListAdapter);

        categoryListAdapter.setCategories((ArrayList<Category>) args.getSerializable("categories"));

        TextView selectingPlayer = (TextView) root.findViewById(R.id.selecting_player);
        if(!args.getBoolean("pick")) {
            selectingPlayer.setText(((Player) args.getSerializable("selector")).getName());
        }else{
            selectingPlayer.setVisibility(GONE);

            TextView categoryTitle = (TextView) root.findViewById(R.id.category_title);
            categoryTitle.setText(getResources().getText(R.string.select_a_category));
        }

        final TimeLimit timeLimit = (TimeLimit) args.getSerializable("timeLimit");
        final int leftAtStart = (int) timeLimit.getLeft();
        final ProgressBar timeLimitDisplay = (ProgressBar) root.findViewById(R.id.timeLimit);
        timeLimitDisplay.setMax(leftAtStart);
        CountDownTimer timeLimitCount = new CountDownTimer(leftAtStart, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLimitDisplay.setProgress((int) millisUntilFinished);
            }

            @Override
            public void onFinish() {
                timeLimitDisplay.setProgress(0);
            }
        }.start();

        return root;
    }

    public void setCategoryInterface(CategoryInterface categoryInterface) {
        this.categoryInterface = categoryInterface;
    }

    @Override
    public void onTextViewClick(Category category) {
        Bundle args = getArguments();

        if(!args.getBoolean("pick")){
            return;
        }

        categoryInterface.onCategorySelected(category);
    }

    public interface CategoryInterface{
        public void onCategorySelected(Category category);
    }
}
