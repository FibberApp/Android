package com.arctro.fibber.views;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;

import com.arctro.fibber.R;
import com.chabbal.slidingdotsplash.SlidingSplashView;

//Tutorials
public class TutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;

        int targetWidth = 1080;
        int targetHeight = 1920;

        float scale = Math.min(targetWidth/screenWidth, targetHeight/screenHeight);

        SlidingSplashView slidingSplashView = (SlidingSplashView) findViewById(R.id.splash);
        slidingSplashView.setLayoutParams(new CoordinatorLayout.LayoutParams((int)(targetWidth * scale), (int)(targetHeight * scale)));
    }

}
