package com.arctro.fibber.supporting;

/**
 * Created by Ben McLean on 6/9/2017.
 */
public class Consts {

    //The default server address
    public static String SERVER_ADDRESS = "ws://192.168.1.12:8080/game";

    //Allow custom server installations
    public static boolean CUSTOM_SERVER_ALLOWED = true;

}
