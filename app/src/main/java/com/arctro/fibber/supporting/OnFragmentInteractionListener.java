package com.arctro.fibber.supporting;

import android.net.Uri;

/**
 * Created by Ben McLean on 6/16/2017.
 */

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Uri uri);
}
