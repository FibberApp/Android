package com.arctro.fibber.supporting;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;

import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Ben McLean on 6/11/2017.
 */

public class Utils {

    /**
     * Shorthand function for sleeping in a non UI thread and then running the runnable in a UI thread
     * @param time Time to sleep for
     * @param after What to run after sleep
     */
    public static void UISleep(final long time, final Runnable after){
        ThreadFactory.queue(Threads.UI_NO_LOCK, new Runnable(){
            public void run(){
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ThreadFactory.queue(Threads.UI, after);
            }
        });
    }

    /**
     * Get the UTC offset
     * @return The UTC offset
     */
    public static long getUTCOffset(){
        return TimeZone.getDefault().getOffset(System.currentTimeMillis());
    }

    /**
     * Format a question
     * @param s The question
     * @param b What to replace the blank with
     * @return The formatted question
     */
    public static String formatQuestion(String s, String b){
        if(s.contains("<BLANK>")) {
            s = s.replaceAll("<BLANK>", b);
        }else{
            s = s + " " + b;
        }

        s = Utils.toTitleCase(s);

        return s;
    }

    /**
     * Formats a string for the UI
     * @param s The string to format
     * @return
     */
    public static String formatUI(String s){
        s = s.replaceAll("<BLANK>", "_____");

        s = Utils.toTitleCase(s);

        return s;
    }

    /**
     * Converts a string to title case
     * @param input The string to convert
     * @return The title cased string
     */
    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    /**
     * Hides the software keyboard
     * @param activity
     * @param v
     */
    public static void hideSoftKeyboard(Activity activity, View v) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                v.getWindowToken(), 0);
    }

    public static class KeyboardCloseOnFocusChange implements View.OnFocusChangeListener{
        Activity a;

        public KeyboardCloseOnFocusChange(Activity a) {
            this.a = a;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            hideSoftKeyboard(a,v);
        }
    }
}
