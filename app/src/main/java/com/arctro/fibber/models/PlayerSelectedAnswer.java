package com.arctro.fibber.models;

import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Holds the player's selected answer payload
 * Created by Ben McLean on 7/6/2017.
 */
public class PlayerSelectedAnswer implements Serializable{
    Answer answer;
    @Nullable
    Player answerer;
    ArrayList<Player> selected;

    public PlayerSelectedAnswer(JSONObject data) throws JSONException{
        JSONObject answerJSON = data.getJSONObject("answer");
        answer = new Answer(answerJSON);

        if(answerJSON.has("answerer")){
            answerer = new Player(answerJSON.getJSONObject("answerer"));
        }

        JSONArray selectedJSON = data.getJSONArray("players");
        selected = new ArrayList<>();
        for(int i = 0; i < selectedJSON.length(); i++){
            selected.add(new Player(selectedJSON.getJSONObject(i)));
        }
    }

    public PlayerSelectedAnswer(Answer answer, Player answerer, ArrayList<Player> selected) {
        this.answer = answer;
        this.answerer = answerer;
        this.selected = selected;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    @Nullable
    public Player getAnswerer() {
        return answerer;
    }

    public void setAnswerer(@Nullable Player answerer) {
        this.answerer = answerer;
    }

    public ArrayList<Player> getSelected() {
        return selected;
    }

    public void setSelected(ArrayList<Player> selected) {
        this.selected = selected;
    }

    public static class Comparator implements java.util.Comparator<PlayerSelectedAnswer>{

        @Override
        public int compare(PlayerSelectedAnswer a, PlayerSelectedAnswer b) {
            return a.getAnswerer() == null ? 0 : -1;
        }
    }
}
