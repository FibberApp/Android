package com.arctro.fibber.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Holds the question payload
 * Created by Ben McLean on 7/5/2017.
 */

public class Question implements Serializable{

    int questionId;
    String question;
    Category category;

    public Question(JSONObject data) throws JSONException{
        questionId = data.getInt("questionId");
        question = data.getString("question");
        category = new Category(data.getJSONObject("category"));
    }

    public Question(int questionId, String question, Category category) {
        this.questionId = questionId;
        this.question = question;
        this.category = category;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
