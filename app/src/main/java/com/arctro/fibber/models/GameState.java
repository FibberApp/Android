package com.arctro.fibber.models;

/**
 * Holds the possible game states
 * Created by Ben McLean on 7/3/2017.
 */
public enum GameState {
    /**
     * The game is in the lobby
     */
    LOBBY,

    /**
     * The categories are being selected
     */
    CATEGORY_SELECT,

    /**
     * The answer is being inputted
     */
    ANSWER_INPUT,

    /**
     * Waiting for all players to input their answers
     */
    ANSWER_INPUT_WAIT,

    /**
     * The answer are being selected
     */
    SELECT_ANSWER,

    /**
     * Waiting for all players to select their answers, showing scoreboard
     */
    SELECT_ANSWER_WAIT,
}
