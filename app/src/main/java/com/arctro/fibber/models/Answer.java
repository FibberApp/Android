package com.arctro.fibber.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Holds the answer payload
 * Created by Ben McLean on 7/5/2017.
 */
public class Answer implements Serializable {

    String answerId;
    String answer;
    boolean enabled;

    public Answer(JSONObject data) throws JSONException{
        answerId = (String) data.get("answerId");
        answer = (String) data.get("answer");
        enabled = true;
    }

    public Answer(String answerId, String answer, boolean enabled) {
        this.answerId = answerId;
        this.answer = answer;
        this.enabled = enabled;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
