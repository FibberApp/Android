package com.arctro.fibber.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Ben McLean on 7/3/2017.
 */

public class Player implements Serializable {
    String sessionId;
    String name;
    String icon;
    int score;

    public Player(JSONObject playerData) throws JSONException{
        sessionId = playerData.getString("sessionId");
        name = playerData.getString("name");
        icon = playerData.getString("icon");
        score = playerData.getInt("score");
    }

    public Player(String sessionId, String name, String icon, int score) {
        this.sessionId = sessionId;
        this.name = name;
        this.icon = icon;
        this.score = score;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return sessionId.equals(player.sessionId);

    }

    @Override
    public int hashCode() {
        return sessionId.hashCode();
    }
}
