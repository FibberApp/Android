package com.arctro.fibber.models;

import android.util.Log;

import com.arctro.fibber.supporting.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Holds the time limit payload
 * Created by Ben McLean on 7/4/2017.
 */

public class TimeLimit implements Serializable{

    long end;

    public TimeLimit(JSONObject data, long diff) throws JSONException{
        this(data.getLong("absolute"), diff);
    }

    public TimeLimit(long absolute, long diff){
        end = absolute - diff;
    }

    public TimeLimit(long end) {
        this.end = end;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public long getLeft(){
        return end - System.currentTimeMillis();
    }
}
