package com.arctro.fibber.models;

import org.json.JSONObject;

/**
 * Holds a command that will be broadcasted or has been recieved
 * Created by Ben McLean on 6/9/2017.
 */
public class Command {
    String command;
    String requestToken;
    JSONObject payload;

    public Command(String command, String requestToken, JSONObject payload) {
        this.command = command;
        this.requestToken = requestToken;
        this.payload = payload;
    }

    public Command(String command, JSONObject payload) {
        this.command = command;
        this.payload = payload;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public JSONObject getPayload() {
        return payload;
    }

    public void setPayload(JSONObject payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "Command{" +
                "command='" + command + '\'' +
                ", requestToken='" + requestToken + '\'' +
                ", payload=" + payload +
                '}';
    }
}
