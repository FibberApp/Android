package com.arctro.fibber.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Holds the the category payload
 * Created by Ben McLean on 7/3/2017.
 */
public class Category implements Serializable{

    int categoryId;
    String name;

    public Category(JSONObject data) throws JSONException {
        this.categoryId = data.getInt("categoryId");
        this.name = data.getString("name");
    }

    public Category(int categoryId, String name) {
        this.categoryId = categoryId;
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
