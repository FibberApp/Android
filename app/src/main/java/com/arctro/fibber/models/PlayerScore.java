package com.arctro.fibber.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Holds the player score payload
 * Created by Ben McLean on 7/5/2017.
 */
public class PlayerScore implements Serializable{
    int scored;
    Player player;

    public PlayerScore(JSONObject data) throws JSONException{
        scored = data.getInt("scored");
        player = new Player(data.getJSONObject("player"));
    }

    public PlayerScore(int scored, Player player) {
        this.scored = scored;
        this.player = player;
    }

    public int getScored() {
        return scored;
    }

    public void setScored(int scored) {
        this.scored = scored;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public static class Comparator implements java.util.Comparator<PlayerScore>{
        @Override
        public int compare(PlayerScore a, PlayerScore b) {
            int as = a.getPlayer().getScore();
            int bs = b.getPlayer().getScore();

            if(as == bs){
                return 0;
            }else if(as > bs){
                return -1;
            }else{
                return 1;
            }
        }
    }
}
