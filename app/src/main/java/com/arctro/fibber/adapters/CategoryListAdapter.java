package com.arctro.fibber.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.models.Category;
import com.arctro.fibber.supporting.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * See AnswerSelectAdapter for basically the same stuff. Adapts a list of categories into a UI list of categories
 * Created by Ben McLean on 7/3/2017.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    //Store the categories
    public List<Category> categories = new ArrayList<>();
    //Accept an interface so the buttons can have actions. This interface interfaces with another class so input from elements generated within this adapter can be used
    CategoryTextListAdapterInterface categoryTextListAdapterInterface = null;

    public CategoryListAdapter(CategoryTextListAdapterInterface categoryTextListAdapterInterface){
        this.categoryTextListAdapterInterface = categoryTextListAdapterInterface;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textView;

        public ViewHolder(View v) {
            super(v);
            this.textView = (TextView) v.findViewById(R.id.category_name);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_text_view, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.textView.setText(Utils.formatUI(categories.get(position).getName()));

        //On click call the provided interface and pass in the category that was selected
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(categoryTextListAdapterInterface != null){
                    categoryTextListAdapterInterface.onTextViewClick(categories.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setCategories(List<Category> categories){
        this.categories = categories;

        //Update the dataset (must be done in UI thread)
        ThreadFactory.queue(Threads.UI, new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    public interface CategoryTextListAdapterInterface{
        public void onTextViewClick(Category category);
    }
}
