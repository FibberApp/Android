package com.arctro.fibber.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.models.Player;
import com.arctro.fibber.models.PlayerScore;

import java.util.ArrayList;
import java.util.List;

/**
 * See AnswerSelectAdapter for basically the same stuff. Adapts a list of player scores into a UI list of players and their scores
 * Created by Ben McLean on 7/3/2017.
 */

public class PlayerScoreListAdapter extends RecyclerView.Adapter<PlayerScoreListAdapter.ViewHolder> {

    //Holds a list of player scores
    public List<PlayerScore> playerScores = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView player;
        public TextView score;

        public ViewHolder(View v) {
            super(v);
            this.player = (TextView) v.findViewById(R.id.player_name);
            this.score = (TextView) v.findViewById(R.id.score);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.scoreboard_player_view, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PlayerScore playerScore = playerScores.get(position);
        Player player = playerScore.getPlayer();

        holder.player.setText(player.getName());
        //Format the scoreboard score so it has "score (scored this round)"
        holder.score.setText(String.format("%d (+%d)", player.getScore(), playerScore.getScored()));
    }

    @Override
    public int getItemCount() {
        return playerScores.size();
    }

    public void setPlayerScores(List<PlayerScore> playerScores){
        this.playerScores = playerScores;

        //Dataset updates must be done in the UI thread
        ThreadFactory.queue(Threads.UI, new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }
}
