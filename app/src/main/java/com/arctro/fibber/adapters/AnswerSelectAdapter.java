package com.arctro.fibber.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.models.Answer;
import com.arctro.fibber.models.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapts a list of answers into buttons that can be selected
 *
 * Created by Ben McLean on 7/3/2017.
 */
public class AnswerSelectAdapter extends RecyclerView.Adapter<AnswerSelectAdapter.ViewHolder> {

    public List<Answer> answers = new ArrayList<>();
    AnswerSelectInterface answerSelectInterface = null;

    /**
     * Create a new AnswerSelectAdapter
     * @param answerSelectInterface Allows the creator of the instance to receive click events
     */
    public AnswerSelectAdapter(AnswerSelectInterface answerSelectInterface){
        this.answerSelectInterface = answerSelectInterface;
    }

    /**
     * Holds the view
     */
    public static class ViewHolder extends RecyclerView.ViewHolder{

        public Button button;

        public ViewHolder(View v) {
            super(v);
            this.button = (Button) v.findViewById(R.id.answer);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.answer_button_view, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Answer answer = answers.get(position);

        holder.button.setText(answer.getAnswer());

        //Check if the button is clickable
        if(!answer.isEnabled()){
            holder.button.setEnabled(false);
            holder.button.setBackgroundColor(Color.DKGRAY);
        }

        //Action listener for button click
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //If interface exists
                if(answerSelectInterface != null){
                    //Call function with answer that was selected
                    answerSelectInterface.onAnswerSelected(answers.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    /**
     * Update the dataset
     * @param answers The answer to update with
     */
    public void setAnswers(List<Answer> answers){
        this.answers = answers;

        //Notify of changes
        ThreadFactory.queue(Threads.UI, new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    /**
     * Allows a parent to interface with its child
     */
    public interface AnswerSelectInterface {
        public void onAnswerSelected(Answer answer);
    }
}
