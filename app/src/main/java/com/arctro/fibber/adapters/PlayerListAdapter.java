package com.arctro.fibber.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arctro.fibber.R;
import com.arctro.fibber.controllers.thread.ThreadFactory;
import com.arctro.fibber.controllers.thread.Threads;
import com.arctro.fibber.models.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * See AnswerSelectAdapter for basically the same stuff. Adapts a list of players into a UI list of players
 * Created by Ben McLean on 7/3/2017.
 */

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder> {

    //Hold a list of players
    public List<Player> players = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textView;
        public ViewHolder(View v) {
            super(v);
            this.textView = (TextView) v.findViewById(R.id.player_name);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_lobby_player_view, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(players.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public void setPlayers(List<Player> players){
        this.players = players;

        //Dataset updates must be done in the UI thread
        ThreadFactory.queue(Threads.UI, new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }
}
