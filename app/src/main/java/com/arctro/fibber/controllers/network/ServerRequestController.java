package com.arctro.fibber.controllers.network;

import android.util.Log;

import com.arctro.fibber.controllers.network.command_listeners.ExceptionCommandListener;
import com.arctro.fibber.controllers.network.command_listeners.ServerTimeCommandListener;
import com.arctro.fibber.models.Command;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.net.URI;
import java.util.UUID;

/**
 * Handles the connection between the client and server, and handles responses.
 *
 * When the client connects to the server the server sends it's local time to the client.
 * The client then uses this time to synchronize it's clocks with the servers. When a
 * command is sent to the server an optional "request token" can accompany it allowing
 * the client to listen to a response to that specific request. This is used because
 * WebSockets do not have request/response, rather just requests. ResponseListeners are
 * attached to the request token and called when their appropriate response is received.
 */
public class ServerRequestController {

    /**
     * Command broadcasted when connection is lost
     */
    public static final String CONN_CLOSED = "internal:conn_closed";

    /**
     * A static instance of ServerRequestController that imitates the dependency injection for the GameActivity
     */
    public static ServerRequestController server = null;

    /**
     * Stores the response listeners associated with a requestToken
     */
    HashMap<String, ResponseListener> responses;
    /**
     * A list of general command listeners
     */
    List<CommandListener> listeners;

    /**
     * The WebSocket that is wrapped by this controller
     */
    public WebSocketClient client;

    /**
     * The difference between the server and internal clock's time (may be inaccurate by some milliseconds)
     */
    long serverTimeDifference;

    /**
     * Create a new ServerRequestController connecting to URL conn
     *
     * @param conn The URL to connect to
     */
    public ServerRequestController(URI conn) {
        //Create a new websocket client
        client = new WebSocketClient(conn, new Draft_17()) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                Log.d("Conn", "Connected");
            }

            @Override
            public void onMessage(String message) {
                try {
                    //Print the message for debug
                    Log.d("SM", message);

                    //Parse the message
                    JSONObject jo = new JSONObject(message);

                    //Create a new command
                    Command command = new Command(jo.getString("command"), jo.has("requestToken") ? jo.getString("requestToken") : "", jo.getJSONObject("payload"));

                    //Check if there are any response listeners waiting for this message
                    ResponseListener response = responses.get(command.getRequestToken());
                    if (response != null) {
                        response.response(command);
                        responses.remove(command.getRequestToken());
                    }

                    //Send message to all listeners
                    for (CommandListener listener : listeners) {
                        listener.receive(command);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                Log.d("Conn", "Disconnected");
                Command command = new Command(CONN_CLOSED, "", new JSONObject());

                for (CommandListener listener : listeners) {
                    listener.receive(command);
                }
            }

            @Override
            public void onError(Exception ex) {
                Log.d("Conn", "Error");
                ex.printStackTrace();
            }
        };
        try {
            client.connectBlocking();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        responses = new HashMap<>();
        listeners = new ArrayList<>();

        //Default listeners
        listeners.add(new ServerTimeCommandListener(this));
        listeners.add(new ExceptionCommandListener());
    }

    /**
     * Send a command to the server
     *
     * @param command The command to send (without a request token)
     */
    public void request(Command command) throws WebsocketNotConnectedException {
        command.setRequestToken("");

        JSONObject jo = new JSONObject();

        try {
            jo.put("command", command.getCommand());
            jo.put("requestToken", "");
            jo.put("payload", command.getPayload());
        } catch (Exception e) {
            return;
        }

        client.send(jo.toString());
    }

    /**
     * Send a command to the server and listen for a response
     *
     * @param command         The command to send (without a request token)
     * @param responseHandler Listen for a response
     */
    public void request(Command command, ResponseListener responseHandler) throws WebsocketNotConnectedException {
        //Check if there is a response handler
        if (responseHandler == null) {
            request(command);
        }

        JSONObject jo = new JSONObject();

        //Get a response token
        String requestToken = generateRequestToken();
        command.setRequestToken(requestToken);
        responses.put(requestToken, responseHandler);

        try {
            jo.put("command", command.getCommand());
            jo.put("requestToken", requestToken);
            jo.put("payload", command.getPayload());
        } catch (Exception e) {
            return;
        }

        client.send(jo.toString());
    }

    /**
     * Add a new command listener
     *
     * @param commandListener The command listener to add
     */
    public void registerCommandListener(CommandListener commandListener) {
        listeners.add(commandListener);
    }

    /**
     * Checks if still connected
     * @return Is still connected
     */
    public boolean isConnected() {
        return client.getConnection().isOpen();
    }

    /**
     * Gets the server time difference
     * @return The server time difference
     */
    public long getServerTimeDifference() {
        return serverTimeDifference;
    }

    public void setServerTimeDifference(long serverTimeDifference) {
        this.serverTimeDifference = serverTimeDifference;
    }

    /**
     * Generates a unique request token. A request token allows the client to identify a response.
     * When the server responds it returns the provided token in its payload
     * @return A unique request token
     */
    private String generateRequestToken() {
        String token;
        do {
            token = UUID.randomUUID().toString();
        } while (responses.containsKey(token));

        return token;
    }

}
