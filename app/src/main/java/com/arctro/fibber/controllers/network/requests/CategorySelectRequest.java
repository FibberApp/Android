package com.arctro.fibber.controllers.network.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Sends a "select_category" request to the server
 * Created by Ben McLean on 6/18/2017.
 */

public class CategorySelectRequest extends Request{

    /**
     * The category that has been selected
     */
    int categoryId;

    public int getCategoryId() {
        return categoryId;
    }

    /**
     * Set the categoryId
     * @param categoryId The value to set it to
     * @return The builder
     */
    public CategorySelectRequest setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    @Override
    public JSONObject buildPayload() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("categoryId", getCategoryId());
        return obj;
    }

    @Override
    public String getCommandName(){
        return Request.CATEGORY_SELECT;
    }
}
