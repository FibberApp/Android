package com.arctro.fibber.controllers.network.requests;

import android.util.Log;

import com.arctro.fibber.controllers.network.ResponseListener;
import com.arctro.fibber.controllers.network.ServerRequestController;
import com.arctro.fibber.models.Command;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A abstract class that provides a template for all command builders to be built off. Every command that can be
 * sent to the server is constructed using a builder decended from this class. This class provides the basic
 * boilerplate functionality required by each request, i.e Setting response listeners, setting the endpoint,
 * and broadcasting the request
 */
public abstract class Request {

    /* COMMAND NAMES */
    public static final String CREATE_GAME = "create_game";
    public static final String JOIN_GAME = "join_game";
    public static final String BUILD_GAME = "build_game";
    public static final String CATEGORY_SELECT = "select_category";
    public static final String INPUT_ANSWER = "input_answer";
    public static final String SELECT_ANSWER = "select_answer";
    public static final String ACKNOWLEDGE = "acknowledge";

    /**
     * Where the command is being sent
     */
    ServerRequestController endpoint;
    /**
     * A response listener
     */
    ResponseListener responseListener = null;

    public Request setResponseListener(ResponseListener responseListener){
        this.responseListener = responseListener;
        return this;
    }

    public Request setEndpoint(ServerRequestController endpoint){
        this.endpoint = endpoint;
        return this;
    }

    /**
     * Returns the name of the command
     * @return The name of the command
     */
    public abstract String getCommandName();

    /**
     * Builds the command payload
     * @return The command payload
     * @throws JSONException The input could not be parsed into a JSONObject
     */
    public abstract JSONObject buildPayload() throws JSONException;

    /**
     * Builds the command
     * @return The command
     * @throws JSONException buildPayload() was unable to parse the payload into a JSONObject
     */
    public Command build() throws JSONException{
        Command command = new Command(getCommandName(), buildPayload());
        return command;
    }

    /**
     * Broadcasts a command to the specified endpoint
     * @throws JSONException buildPayload() was unable to parse the payload into a JSONObject
     */
    public void broadcast() throws JSONException{
        Log.d("b", build().toString());

        //Check if still connected to the server
        if(!endpoint.isConnected()){
            return;
        }

        //Check if a response listener has been specified
        if(responseListener != null){
            endpoint.request(build(), responseListener);
        }else{
            endpoint.request(build());
        }
    }

}
