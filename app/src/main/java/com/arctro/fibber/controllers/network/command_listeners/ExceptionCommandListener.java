package com.arctro.fibber.controllers.network.command_listeners;

import android.util.Log;

import com.arctro.fibber.controllers.network.CommandListener;
import com.arctro.fibber.models.Command;

/**
 * Listens for exceptions
 * Created by Ben McLean on 7/13/2017.
 */
public class ExceptionCommandListener implements CommandListener {

    @Override
    public void receive(Command command) {
        //On exception received print the exception
        if (command.getCommand().equals("exception")) {
            Log.e("Server Exception", command.getPayload().toString());
        }
    }
}
