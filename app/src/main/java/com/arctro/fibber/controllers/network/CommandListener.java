package com.arctro.fibber.controllers.network;

import com.arctro.fibber.models.Command;

/**
 * Allows a class to listen for commands that are received from the server by the ServerRequestController
 */
public interface CommandListener {

    /**
     * When a command is received this function is called with the command
     * @param command The received command
     */
    public void receive(Command command);

}
