package com.arctro.fibber.controllers.network.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Sends a "build_game" command to the server
 * Created by Ben McLean on 6/18/2017.
 */
public class BuildGameRequest extends Request{

    @Override
    public JSONObject buildPayload() throws JSONException {
        JSONObject obj = new JSONObject();
        return obj;
    }

    @Override
    public String getCommandName(){
        return Request.BUILD_GAME;
    }
}
