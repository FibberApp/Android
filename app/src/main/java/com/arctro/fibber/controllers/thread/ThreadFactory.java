package com.arctro.fibber.controllers.thread;

import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A centralized thread queue that contains threads for specific purposes.
 * Created by Ben McLean on 6/10/2017.
 */
public class ThreadFactory {

    /**
     * A dictionary of each thread's queue
     */
    private static HashMap<String, List<Runnable>> runnables = new HashMap<String, List<Runnable>>();
    /**
     * A dictionary of each thread, associated with its name
     */
    private static HashMap<String, QueueThread> threads = new HashMap<String, QueueThread>();
    static{
        //Start the thread required for the startup to do the rest of the startup
        ThreadFactory.start(Threads.UI_NO_LOCK);
        ThreadFactory.start(Threads.PROCESS);
    }

    /**
     * Creates a new thread queue
     * @param name The name of the thread queue
     */
    public static void start(String name){
        QueueThread qt = new QueueThread(name);
        qt.start();
        threads.put(name, qt);
        runnables.put(name, new ArrayList<Runnable>());
    }

    /**
     * Stops an existing thread queue
     * @param name The name of the thread queue to stop
     */
    public static void stop(String name){
        QueueThread t = threads.get(name);

        //Sanity check
        if(t != null){
            t.setRunning(false);
        }
    }

    /**
     * Queues a new runnable on the specified thread
     * @param thread The thread queue to use
     * @param runnable The runnable to queue
     */
    public static void queue(String thread, Runnable runnable){
        //Add UI runnables to the System's UI thread queue
        if(thread.equals(Threads.UI)){
            new Handler(Looper.getMainLooper()).post(runnable);
        }

        List<Runnable> runnableList = runnables.get(thread);

        //Sanity check
        if(runnableList == null){
            return;
        }

        runnableList.add(runnable);
    }

    /**
     * Manages a queue
     */
    private static class QueueThread extends Thread{

        boolean running = true;
        String name;

        public QueueThread(String name){
            this.name = name;
        }

        @Override
        public void run(){
            while(running) {
                //Gets the queue
                List<Runnable> runnableList = runnables.get(name);

                //Checks if the queue exists and has items
                if(runnableList == null || runnableList.isEmpty()){
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    continue;
                }

                //Process queue
                for(int i = 0; i < runnableList.size(); i++){
                    runnableList.get(i).run();
                    runnableList.remove(i);
                }
            }
        }

        public boolean isRunning() {
            return running;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        public String getThreadName() {
            return name;
        }

        public void setThreadName(String name) {
            this.name = name;
        }
    }
}
