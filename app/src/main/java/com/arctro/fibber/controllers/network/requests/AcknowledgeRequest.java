package com.arctro.fibber.controllers.network.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Sends an "acknowledge" command to the server
 * Created by Ben McLean on 7/7/2017.
 */

public class AcknowledgeRequest extends Request {

    @Override
    public JSONObject buildPayload() throws JSONException {
        JSONObject obj = new JSONObject();
        return obj;
    }

    @Override
    public String getCommandName() {
        return Request.ACKNOWLEDGE;
    }

}