package com.arctro.fibber.controllers.network;

import com.arctro.fibber.models.Command;

/**
 * Listens for a response from the server from a request broadcasted
 */
public interface ResponseListener {

    /**
     * When a response is received this function is called with it
     * @param command The response
     */
    public void response(Command command);

}
