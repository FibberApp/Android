package com.arctro.fibber.controllers.network.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ben McLean on 6/18/2017.
 */

public class SelectAnswerRequest extends Request{

    String answerId;

    public String getAnswerId() {
        return answerId;
    }

    public SelectAnswerRequest setAnswerId(String answerId) {
        this.answerId = answerId;
        return this;
    }

    @Override
    public JSONObject buildPayload() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("answerId", answerId);
        return obj;
    }

    @Override
    public String getCommandName(){
        return Request.SELECT_ANSWER;
    }
}
