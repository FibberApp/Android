package com.arctro.fibber.controllers.thread;

/**
 * Holds the default thread names
 * Created by Ben McLean on 6/10/2017.
 */
public class Threads {

    public static final String UI = "ui";
    public static final String UI_NO_LOCK = "ui_nl";
    public static final String PROCESS = "process";

}
