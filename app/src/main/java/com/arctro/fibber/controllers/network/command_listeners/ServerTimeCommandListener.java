package com.arctro.fibber.controllers.network.command_listeners;

import android.util.Log;

import com.arctro.fibber.controllers.network.CommandListener;
import com.arctro.fibber.controllers.network.ServerRequestController;
import com.arctro.fibber.models.Command;

import org.json.JSONException;

/**
 * Listens for the server time command for syncing the clock
 * Created by Ben McLean on 7/13/2017.
 */

public class ServerTimeCommandListener implements CommandListener{

    ServerRequestController serverRequestController;

    public ServerTimeCommandListener(ServerRequestController serverRequestController) {
        this.serverRequestController = serverRequestController;
    }

    @Override
    public void receive(Command command) {
        try {
            //On server time received update the server time difference variable
            if (command.getCommand().equals("server_time")) {
                serverRequestController.setServerTimeDifference(command.getPayload().getLong("serverTime") - System.currentTimeMillis());

                Log.e("serverTimeDiff", serverRequestController.getServerTimeDifference() + "");
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
