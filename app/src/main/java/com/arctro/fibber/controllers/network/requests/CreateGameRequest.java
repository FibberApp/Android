package com.arctro.fibber.controllers.network.requests;

import com.arctro.fibber.models.Command;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Sends a "create_game" request to the server
 * Created by Ben McLean on 6/18/2017.
 */

public class CreateGameRequest extends Request{

    /**
     * The name of the user
     */
    String name;

    /**
     * Returns the inputted name
     * @return The inputted name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name
     * @param name The name
     * @return The builder
     */
    public CreateGameRequest setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public JSONObject buildPayload() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("name", getName());
        return obj;
    }

    @Override
    public String getCommandName(){
        return Request.CREATE_GAME;
    }
}
