package com.arctro.fibber.controllers.network.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Sends an "input_answer" command to the server
 * Created by Ben McLean on 6/18/2017.
 */

public class InputAnswerRequest extends Request{

    /**
     * The answer that was inputted
     */
    String answer;

    /**
     * Return the inputted answer
     * @return The answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * Set the answer
     * @param answer The answer
     * @return The builder
     */
    public InputAnswerRequest setAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    @Override
    public JSONObject buildPayload() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("answer", getAnswer());
        return obj;
    }

    @Override
    public String getCommandName(){
        return Request.INPUT_ANSWER;
    }
}
