package com.arctro.fibber.controllers.network.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *  Sends a "join_game" command to the server.
 * Created by Ben McLean on 6/18/2017.
 */

public class JoinGameRequest extends Request{

    /**
     * The name of the player
     */
    String name;
    /**
     * The game code of the lobby
     */
    String gameCode;

    public String getName() {
        return name;
    }

    public JoinGameRequest setName(String name) {
        this.name = name;
        return this;
    }

    public String getGameCode() {
        return gameCode;
    }

    public JoinGameRequest setGameCode(String gameCode) {
        this.gameCode = gameCode;
        return this;
    }

    @Override
    public JSONObject buildPayload() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("name", getName());
        obj.put("gameCode", getGameCode());
        return obj;
    }

    @Override
    public String getCommandName(){
        return Request.JOIN_GAME;
    }
}
