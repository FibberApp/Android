package com.arctro.fibber.controllers;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * A centralized singleton to play music
 * Created by Ben McLean on 7/7/2017.
 */
public class MusicService {

    private static MusicService ms;

    /**
     * Create a new instance. Must be called once at the start of the program
     * @param context The application context
     * @return The music service instance
     */
    public static MusicService createInstance(Context context){
        return ms = new MusicService(context);
    }

    /**
     * Returns the current music service instance
     * @return The current music service instance
     */
    public static MusicService getInstance(){
        return ms;
    }

    Context context;
    MediaPlayer current = null;

    /**
     * Init the music service
     * @param context Init the music service
     */
    private MusicService(Context context) {
        this.context = context;
    }

    /**
     * Stops the current track and plays the provided one
     * @param track The provided track
     */
    public void play(int track){
        if(current != null){
            current.stop();
            current.release();
        }

        current = MediaPlayer.create(context, track);
        current.setLooping(true);
        current.start();
    }

    /**
     * Pause the playing track
     */
    public void pause(){
        current.pause();
    }

    /**
     * Unpause the playing track
     */
    public void unpause(){
        current.start();
    }

    /**
     * Stop the music
     */
    public void stop(){
        current.stop();
        current.release();
    }
}
